import java.io.InputStream;
import java.util.*;

public class Controller {

    List<Bunny> bunnyList = new ArrayList<>();

    public void start() {
        readFile();
        printAllBunnies();
    }

    private void readFile() {
        InputStream inputStream = getClass().getResourceAsStream("/data/bunnies.txt");

        Scanner scanner = new Scanner(inputStream, "UTF-8");

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();

            if (!line.isEmpty()) {
                String[] array = line.split(";");

                String name = array[0];
                int age = Integer.parseInt(array[1]);
                boolean lovesEaster = Boolean.parseBoolean(array[2]);

                Bunny bunny = new Bunny(name, age, lovesEaster);

                bunnyList.add(bunny);
            }
        }
    }


    private void printAllBunnies() {
        for (Bunny bunny : bunnyList) {
            System.out.println(bunny);
        }
    }
}
